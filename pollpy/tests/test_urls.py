from django.test import SimpleTestCase
from django.urls import reverse, resolve
from pollpy.views import polls_list, poll_detail, poll_vote, \
    add_poll, edit_poll, edit_choice, delete_poll, delete_choice


class TestUrls(SimpleTestCase):
    def test_polls_list_url(self):
        url = reverse('pollpy:list')
        self.assertEquals(resolve(url).func, polls_list)

    def test_poll_detail_url(self):
        url = reverse('pollpy:detail', args=[2])
        self.assertEquals(resolve(url).func, poll_detail)

    def test_vote_url(self):
        url = reverse('pollpy:vote', args=[1])
        self.assertEquals(resolve(url).func, poll_vote)

    def test_add_poll_url(self):
        url = reverse('pollpy:add')
        self.assertEquals(resolve(url).func, add_poll)

    def test_edit_poll_url(self):
        url = reverse('pollpy:edit_poll', args=[3])
        self.assertEquals(resolve(url).func, edit_poll)

    def test_edit_choice_url(self):
        url = reverse('pollpy:edit_choice', args=[3])
        self.assertEquals(resolve(url).func, edit_choice)

    def test_delete_poll_url(self):
        url = reverse('pollpy:delete_poll', args=[3])
        self.assertEquals(resolve(url).func, delete_poll)

    def test_delete_choice_url(self):
        url = reverse('pollpy:delete_choice', args=[3])
        self.assertEquals(resolve(url).func, delete_choice)