from django.contrib.auth.models import User
from django.test import TestCase, Client
from django.urls import reverse
from pollpy.models import Poll, Choice


class TestViews(TestCase):

    def setUp(self):
        self.client = Client()

        # Benötigte Modelle werden für den Test erstellt
        self.user = User.objects.create_user('testuser', 'test@test.com', 'polllisttest')
        self.user = User.objects.create_user('testuser2', 'test@test.com', 'polllisttest')
        self.client.login(username='testuser', password='polllisttest')
        self.poll = Poll.objects.create(text='Testfrage', pub_date='2019-11-03', author_id=1)
        self.choice = Choice.objects.create(choice_text='Testantwort', poll_id=1)

        self.list_url = reverse('pollpy:list')
        self.detail_url = reverse('pollpy:detail', args=[self.poll.id])
        self.delete_choice_url = reverse('pollpy:delete_choice', args=[1])
        self.add_poll_url = reverse('pollpy:add')

    def test_poll_list_GET(self):
        response = self.client.get(self.list_url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'pollpy/polls_list.html')

    def test_poll_detail_GET(self):
        response = self.client.get(self.detail_url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'pollpy/poll_detail.html')

    def test_add_poll_GET(self):
        response = self.client.get(self.add_poll_url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'pollpy/add_poll.html')
