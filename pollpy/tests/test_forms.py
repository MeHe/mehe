from django.test import SimpleTestCase
from pollpy.forms import PollForm, EditPollForm, EditChoiceForm


class TestForms(SimpleTestCase):

    def test_poll_form_valid_data(self):
        form_data = {
            'text': 'Das ist ein Formtest',
            'choice_text1': 'Antwort',
            'choice_text2': 'Antwort2',
        }
        form = PollForm(data=form_data)
        self.assertTrue(form.is_valid())

    def test_poll_form_no_data(self):
        form = PollForm(data={})
        self.assertFalse(form.is_valid())
        self.assertEquals(len(form.errors), 3)

    def test_poll_form_only_text(self):
        form_data = {
            'text': 'Das ist ein Formtest',
        }
        form = PollForm(data=form_data)
        self.assertFalse(form.is_valid())
        self.assertEquals(len(form.errors), 2)

    def test_poll_form_only_choices(self):
        form_data = {
            'choice_text1': 'Antwort',
            'choice_text2': 'Antwort2'
        }
        form = PollForm(data=form_data)
        self.assertFalse(form.is_valid())
        self.assertEquals(len(form.errors), 1)
