import datetime
from django.conf import settings
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models import Count
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render, redirect
from django.urls import reverse
from .models import Poll, Choice, Vote
from .forms import PollForm, EditPollForm, EditChoiceForm


# Create your views here.
@login_required()
def polls_list(request):
    """
    Übersicht über alle Fragen
    :param request:
    :return: polls_list.html
    """
    polls = Poll.objects.all()
    # Filter für die Fragenübersicht
    if 'title' in request.GET:
        polls = polls.order_by('text')
    if 'pub_date' in request.GET:
        polls = polls.order_by('-pub_date')
    if 'trends' in request.GET:
        polls = polls.annotate(Count('vote')).order_by('-vote__count')
    if 'search' in request.GET:
        searched_string = request.GET['search']
        polls = polls.filter(text__icontains=searched_string)

    # Aufteilen der Fragen in mehrere Seiten
    paginator = Paginator(polls, 5)
    page = request.GET.get('page')
    polls = paginator.get_page(page)

    dict_copy = request.GET.copy()
    parameter = dict_copy.pop('page', True) and dict_copy.urlencode()

    context = {
        'polls': polls,
        'parameter': parameter,
    }
    return render(request, 'pollpy/polls_list.html', context)


@login_required()
def add_poll(request):
    """
    Form wird eingelesen und der Titel der Frage wird gespeichert.
    Dabei wird Form nicht direkt gespeichert, um das Erstellungsdatum
    automatisch einzutragen. Daraufhin werden die Antworten gespeichert.
    Beim erstmaligen Erstellen einer Frage kann man zwei Antwortmöglichkeiten
    festlegen und anschließend unter edit_poll weitere Antworten hinzufügen.
    :param request:
    :return: add_poll.html
    """
    if request.method == 'POST':
        form = PollForm(request.POST)

        # Wenn neue Frage erstellt wird, wird das aktuelle Datum eingetragen.
        if form.is_valid():
            new_poll = form.save(commit=False)
            new_poll.pub_date = datetime.datetime.now()
            new_poll.author = request.user
            new_poll.save()
            new_choice1 = Choice(
                poll=new_poll,
                choice_text=form.cleaned_data['choice_text1']
            ).save()
            new_choice2 = Choice(
                poll=new_poll,
                choice_text=form.cleaned_data['choice_text2']
            ).save()
            messages.success(request, 'Ihre Frage wurde erstellt',
                             extra_tags='alert alert-dismissible fade show alert-warning')
            return redirect('pollpy:list')
    else:
        form = PollForm()
    context = {'form': form}
    return render(request, 'pollpy/add_poll.html', context)


@login_required()
def poll_detail(request, poll_id):
    """
    Detailansicht für Fragen, die noch nicht beantwortet wurden.
    Votingfunktion kann nur einmal genutzt werden, sodass Nutzer
    bei nächstem Seitenaufruf auf die Ergebnisseite weitergeleitet
    werden.
    :param request:
    :param poll_id:
    :return: poll_results.html
    """
    poll = get_object_or_404(Poll, id=poll_id)
    if not poll.only_vote_once(request.user):
        results = poll.get_choice_results_dict()
        return render(request, 'pollpy/poll_results.html', context={'poll': poll, 'results': results})
    else:
        only_vote_once = poll.only_vote_once(request.user)
        context = {'poll': poll, 'only_vote_once': only_vote_once}
        return render(request, 'pollpy/poll_detail.html', context)


@login_required()
def poll_vote(request, poll_id):
    """
    Überprüfungen der Votingfunktion verhindern erneutes abstimmen
    und sichert, dass eine Auswahl getroffen wurde.
    :param request:
    :param poll_id:
    :return: poll_results.html
    """
    poll = get_object_or_404(Poll, id=poll_id)

    # Verhindert erneutes Abstimmen durch only_vote_once
    if not poll.only_vote_once(request.user):
        results = poll.get_choice_results_dict()
        messages.error(request,
                       'Sie haben bereits auf diese Frage geantwortet.',
                       extra_tags='alert alert-dismissible fade show alert-warning')
        return HttpResponseRedirect(reverse('pollpy:detail', args=(poll_id,)))

    # Ausgewählter Radio-input
    choice_id = request.POST.get('choice')

    # Sichert, dass Auswahl getroffen wurde
    if choice_id:
        choice = Choice.objects.get(id=choice_id)
        vote = Vote(user=request.user, poll=poll, choice=choice)
        vote.save()
        results = poll.get_choice_results_dict()
    else:
        messages.error(request,
                       'Keine Auswahl getroffen.',
                       extra_tags='alert alert-dismissible fade show alert-warning')
        return HttpResponseRedirect(reverse('pollpy:detail', args=(poll_id,)))
    return render(request, 'pollpy/poll_results.html', context={'poll': poll, 'results': results})


@login_required()
def edit_poll(request, poll_id):
    """
    Nachträgliches bearbeiten nur durch den Autor
    (bzw. Admin im Adminbereich) der Frage möglich.
    :param request:
    :param poll_id:
    :return: edit_poll.html
    """
    poll = get_object_or_404(Poll, id=poll_id)

    # Überprüfen ob angemeldeter Nutzer der Autor ist
    if request.user != poll.author:
        return redirect('index')

    """
    Fragentitel wird immer gespeichert und zusätzliche 
    Antwortmöglichkeit nur bei Bedarf
    """
    if request.method == "POST":
        form = EditPollForm(request.POST, instance=poll)
        if form.is_valid():
            form.save()
            new_choice = Choice(
                poll=poll,
                choice_text=form.cleaned_data['choice_text_add']
            )
            if new_choice.choice_text != '':
                new_choice.save()
            messages.success(request, 'Ihre Frage wurde bearbeitet.',
                            extra_tags='alert alert-dismissible fade show alert-warning')
    else:
        form = EditPollForm(instance=poll)
        context = {'form': form}
    return render(request, 'pollpy/edit_poll.html', {'form': form, 'poll':poll})


@login_required()
def edit_choice(request, choice_id):
    """
    Verändern einer Antwortmöglickeit durch den Autor.
    :param request:
    :param choice_id:
    :return: edit_poll.html
    """
    choice = get_object_or_404(Choice, id=choice_id)

    if request.user != choice.poll.author:
        return redirect('index')

    if request.method == "POST":
        form = EditChoiceForm(request.POST, instance=choice)
        if form.is_valid():
            form.save()
        messages.success(request, 'Ihre Antwort wurde bearbeitet.',
                         extra_tags='alert alert-dismissible fade show alert-warning')
        return HttpResponseRedirect(reverse('pollpy:edit_poll', args=(choice.poll.id,)))
    else:
        form = EditChoiceForm(instance=choice)
    return render(request, 'pollpy/edit_choice.html', {'form':form, 'choice': choice})


@login_required()
def delete_choice(request, choice_id):
    """Löschfunktion für Antwortmöglichkeiten."""
    choice = get_object_or_404(Choice, id=choice_id)

    if request.user != choice.poll.author:
        return redirect('index')

    else:
        choice.delete()
        messages.success(request, 'Die ausgewählte Antwort wurde gelöscht.',
                         extra_tags='alert alert-dismissible fade show alert-success')
        return HttpResponseRedirect(reverse('pollpy:edit_poll', args=(choice.poll.id,)))


def delete_poll(request, poll_id):
    """Löschfunktion für Fragen"""
    poll = get_object_or_404(Poll, id=poll_id)

    if request.user != poll.author:
        return redirect('index')

    else:
        poll.delete()
        messages.success(request, 'Die ausgewählte Frage wurde gelöscht.',
                         extra_tags='alert alert-dismissible fade show alert-success')
        return redirect('pollpy:list')