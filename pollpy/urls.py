from django.urls import path
from . import views

app_name = "pollpy"
urlpatterns = [
    path('list/', views.polls_list, name="list"),
    path('detail/<int:poll_id>/', views.poll_detail, name="detail"),
    path('detail/<int:poll_id>/vote', views.poll_vote, name="vote"),
    path('add/', views.add_poll, name="add"),
    path('edit/poll/<int:poll_id>', views.edit_poll, name="edit_poll"),
    path('edit/choice/<int:choice_id>', views.edit_choice, name="edit_choice"),
    path('delete/poll/<int:poll_id>', views.delete_poll, name="delete_poll"),
    path('delete/choice/<int:choice_id>', views.delete_choice, name="delete_choice"),
]
