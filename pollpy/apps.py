from django.apps import AppConfig


class PollpyConfig(AppConfig):
    name = 'pollpy'
