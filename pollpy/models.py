from django.contrib.auth.models import User
from django.db import models


class Poll(models.Model):
    """Model für einzelne Umfragen."""
    author = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        default=1
    )
    text = models.CharField(max_length=200)
    pub_date = models.DateField()

    def __str__(self):
        return self.text

    def only_vote_once(self, user):
        """
        Einmaliges Bewerten einer Frage ermöglichen.
        :param user:
        :return: boolean
        """
        user_votes = user.vote_set.all()
        qs = user_votes.filter(poll=self)
        if qs.exists():
            return False
        return True

    @property
    def participant_count(self):
        """
        Ausgabe der Teilnehmeranzahl an einer Frage.
        """
        return self.vote_set.count()

    def get_choice_results_dict(self):
        """
        Ausgabe der notwendigen Informationen
        für Visualisierung als Dictionary.
        :return:
        """
        results = []
        for choice in self.choice_set.all():
            c_dict = {}
            c_dict['choice_text'] = choice.choice_text
            c_dict['vote_count'] = choice.vote_count
            if not self.participant_count:
                c_dict['percentage'] == int(0)
            else:
                c_dict['percentage'] = int(choice.vote_count / self.participant_count * 100)
            results.append(c_dict)
        return results


class Choice(models.Model):
    """
    Model für Antwortmöglichkeiten.
    Hat eine Many-to-One-Beziehung zu Poll.
    """
    poll = models.ForeignKey(
        Poll,
        on_delete=models.CASCADE
    )
    choice_text = models.CharField(max_length=150)

    def __str__(self):
        return f"Frage: {self.poll.text[:25]} Antwort: {self.choice_text[:25]}"

    @property
    def vote_count(self):
        """
        Ausgabe der Abstimmungsanzahl für Antwort.
        """
        return self.vote_set.count()


class Vote(models.Model):
    """
    Model, um Bewertungen an Nutzer zu binden.
    """
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    poll = models.ForeignKey(Poll, on_delete=models.CASCADE)
    choice = models.ForeignKey(Choice, on_delete=models.CASCADE)
