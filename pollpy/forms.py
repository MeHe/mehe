from django import forms
from django.utils.translation import ugettext_lazy as _
from .models import Poll, Choice


class PollForm(forms.ModelForm):
    """
    Form für das erstellen von Fragen
    """
    choice_text1 = forms.CharField(label='Erste Antwortmöglickeit', max_length=100, min_length=2,
                                   widget=forms.TextInput(attrs={'class': 'form-control'}))
    choice_text2 = forms.CharField(label='Zweite Antwortmöglichkeit', max_length=100, min_length=2,
                                   widget=forms.TextInput(attrs={'class': 'form-control'}))

    class Meta:
        model = Poll
        fields = ['text', 'choice_text1', 'choice_text2']
        widgets = {
            'text': forms.Textarea(attrs={'class': 'form-control', 'rows': 3, 'cols': 20}),
        }


class EditPollForm(forms.ModelForm):
    choice_text_add = forms.CharField(required=False, label='Weitere Antwortmöglickeiten hinzufügen', max_length=100,
                                      min_length=2, widget=forms.TextInput(attrs={'class': 'form-control'}))

    class Meta:
        model = Poll
        fields = ['text', 'choice_text_add']
        widgets = {
            'text': forms.Textarea(attrs={'class': 'form-control', 'required': 'false', 'rows': 3, 'cols': 20}),
        }
        labels = {
            'text': _('Aktuelle Frage'),
        }


class EditChoiceForm(forms.ModelForm):
    class Meta:
        model = Choice
        fields = ['choice_text', ]
        labels = {
            'choice_text':_('Aktuelle Antwort'),
        }
        widgets = {
            'choice_text': forms.TextInput(attrs={'class': 'form-control', 'required': 'false', 'rows': 2, 'cols': 20,})
        }
