Innerhalb dieses Projekts habe ich pipenv verwendet, sodass zunächst die Befehle
 "pipenv shell" und "pipenv sync" ausgeführt werden müssen. Anschließend kann
 mit "python manage.py runserver" die Anwendung gestartet werden und im Browser
 unter der Adresse "http://127.0.0.1:8000/" gefunden werden.