from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.shortcuts import render, redirect
from django.urls import reverse
from django.http import HttpResponse, HttpResponseRedirect
from .forms import UserRegistrationForm


# Create your views here.
def user_login(request):
    if request.method == "POST":
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(request,
                            username=username,
                            password=password)
        # Login und Weiterleitung zu ursprünglicher URL
        if user is not None:
            login(request, user)
            redirect_url = request.GET.get('next', 'pollpy:list')
            return redirect(redirect_url)
        else:
            messages.error(request,
                           'Ihre Anmeldedaten sind nicht richtig.',
                           extra_tags='alert alert-dismissible fade show alert-danger')
    return render(request, 'users/login.html', context={})


def user_logout(request):
    logout(request)
    return redirect('index')


def user_registration(request):
    if request.method == 'POST':
        form = UserRegistrationForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            password2 = form.cleaned_data['password2']
            email = form.cleaned_data['email']
            user = User.objects.create_user(username, email=email, password=password)
            messages.success(request,
                             'Nun sind Sie ein Teil von Pollpy.',
                             extra_tags='alert alert-dismissible fade show alert-warning')
            return redirect('users:login')
    else:
        form = UserRegistrationForm()

    return render(request, 'users/register.html', {'form': form})
