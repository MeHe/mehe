from django import forms
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError


class UserRegistrationForm(forms.Form):
    username = forms.CharField(label='Benutzername', max_length=25,
        widget=forms.TextInput(attrs={'class':'form-control', 'placeholder': 'Benutzername'}))
    email = forms.EmailField(label='E-Mail',
        widget=forms.TextInput(attrs={'class':'form-control', 'placeholder': 'E-Mail'}))
    password = forms.CharField(
        label='Passwort', max_length=100, min_length=5,
        widget=forms.PasswordInput(attrs={'class':'form-control', 'placeholder': 'Passwort'}))
    password2 = forms.CharField(
        label='Passwort bestätigen', max_length=100,
        widget=forms.PasswordInput(attrs={'class':'form-control', 'placeholder': 'Passwort bestätigen'}))

    def clean_email(self):
        """
        Überprüfen, ob E-Mail bereits verwendet wird.
        :return: email
        """
        email = self.cleaned_data['email']
        qs = User.objects.filter(email=email)
        if qs.exists():
            raise ValidationError(
                'E-Mail wird bereits verwendet.'
            )
        return email

    def clean_username(self):
        """
        Überprüfen, ob Benutzername bereits verwendet wird.
        :return: username
        """
        username = self.cleaned_data['username']
        qs = User.objects.filter(username=username)
        if qs.exists():
            raise ValidationError(
                'Benutzername wird bereits verwendet.'
            )
        return username

    def clean(self):
        """
        Passwortbestätigung.
        """
        cleaned_data = super().clean()
        p1 = cleaned_data.get('password')
        p2 = cleaned_data.get('password2')
        if p1 != p2:
            raise ValidationError(
                'Passwordbestätigung fehlgeschlagen.'

            )